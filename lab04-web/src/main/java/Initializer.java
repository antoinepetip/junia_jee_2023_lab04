import junia.lab04.core.config.AppConfig;
import junia.lab04.core.config.DBConfig;
import junia.lab04.web.config.WebConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class Initializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger( Initializer.class);

    @Override
    protected Class<?>[] getRootConfigClasses() {
        Class[] configs = new Class[2];

        configs[0] = AppConfig.class;
        configs[1] = DBConfig.class;

        return configs;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        Class[] config = new Class[1];

        config[0] = WebConfig.class;

        return config;
    }

    @Override
    protected String[] getServletMappings() {
        String[] mapping = new String[1];

        mapping[0] = "/";

        return mapping;
    }
}
