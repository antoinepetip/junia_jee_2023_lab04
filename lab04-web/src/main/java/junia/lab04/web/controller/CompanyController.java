package junia.lab04.web.controller;

import junia.lab04.core.entity.Company;
import junia.lab04.core.service.CompanyService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


@Controller
public class CompanyController {

    private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger( CompanyController.class);

    CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getListOfCompanies (ModelMap modelMap){
        List<Company> companies = companyService.findAllWithProjects();
        modelMap.addAttribute("companies", companies);
        LOGGER.info("getListOfCompanies");
        LOGGER.info("found {} results", companies.size());
        return "companiesList";
    }

    @RequestMapping("/form")
    public String getForm(ModelMap modelMap){
        Company company = new Company();
        modelMap.addAttribute("company", company);
        LOGGER.info("getForm");
        return "companyForm";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String submitForm(@ModelAttribute("company") Company company){
        companyService.save(company);
        LOGGER.info("submitForm");
        return "redirect:list";
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public String deleteCompany(@PathVariable("id") Long id){
        companyService.deleteById(id);
        LOGGER.info("deleteCompany");
        return "redirect:../list";
    }
}
